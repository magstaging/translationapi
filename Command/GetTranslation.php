<?php

namespace Mbs\TranslationApi\Command;

use Magento\Framework\Exception\LocalizedException;
use Mbs\TranslationApi\Api\TranslationMessageRepositoryInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class GetTranslation extends Command
{
    /**
     * @var TranslationMessageRepositoryInterface
     */
    private $translationMessageRepository;
    /**
     * @var \Magento\Framework\App\State
     */
    private $state;

    public function __construct(
        TranslationMessageRepositoryInterface $translationMessageRepository,
        string $name = null
    ) {
        parent::__construct($name);
        $this->translationMessageRepository = $translationMessageRepository;
    }

    protected function configure()
    {
        $this->setName('mbs:translation:find');
        $this->setDescription('Find the translation for a given message');

        $this->addArgument('storeId', InputArgument::REQUIRED);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try {
            $translation = $this->translationMessageRepository->getTranslationList(
                $input->getArgument('storeId')
            );

            $output->writeln(print_r($translation));
        } catch (LocalizedException $e) {

        }

        $output->writeln('translation found');
    }
}
