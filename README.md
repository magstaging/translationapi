# README #

Add an API to Magento to query the messages Magento has

### How do I get set up? ###

1. clone the repository
2. create folder app/code/Mbs/TranslationApi when located at the root of the Magento site
3. copy the content of this repository within the folder
4. install the module php bin/magento setup:upgrade

Submit in Postman a get url like below:

retrieve a single translation:
http://<magento_url/index.php/rest/V1/gettranslation?message=The requested qty is not available&storeId=1

retrieve all the translation:
http://<magento_url/index.php/rest/V1/getalltranslations?storeId=1