<?php

namespace Mbs\TranslationApi\Api;

interface TranslationMessageRepositoryInterface
{
    /**
     * @param string $message
     * @param int $storeId
     * @return string
     */
    public function getTranslationForMessage($message, $storeId);

    /**
     * @param int $storeId
     * @return []
     */
    public function getTranslationList($storeId);
}