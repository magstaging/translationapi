<?php

namespace Mbs\TranslationApi\Model;

use Magento\Framework\App\Area;

class MessageTranslationFinder implements \Mbs\TranslationApi\Api\TranslationMessageRepositoryInterface
{
    /**
     * @var \Magento\Store\Model\App\Emulation
     */
    private $emulation;
    /**
     * @var \Magento\Framework\App\State
     */
    private $state;
    /**
     * @var \Magento\Framework\Phrase\Renderer\Translate
     */
    private $translate;
    /**
     * @var \Mbs\TranslationApi\Logger
     */
    private $logger;
    /**
     * @var \Magento\Framework\Phrase\Renderer\Translate
     */
    private $rendererTranslate;

    public function __construct(
        \Magento\Store\Model\App\Emulation $emulation,
        \Magento\Framework\App\State $state,
        \Magento\Framework\Phrase\Renderer\Translate $rendererTranslate,
        \Magento\Framework\TranslateInterface $translate,
        \Mbs\TranslationApi\Logger $logger
    ) {
        $this->emulation = $emulation;
        $this->state = $state;
        $this->translate = $translate;
        $this->logger = $logger;
        $this->rendererTranslate = $rendererTranslate;
    }

    /**
     * @inheritDoc
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Exception
     */
    public function getTranslationForMessage($message, $storeId)
    {
        $this->initialiseAreaCode();

        try {
            $this->emulation->startEnvironmentEmulation($storeId, Area::AREA_FRONTEND, true);
            $translation = $this->rendererTranslate->render([$message], []);
            $this->emulation->stopEnvironmentEmulation();
        } catch (\Exception $e) {
            $translation = $message;
            $this->emulation->stopEnvironmentEmulation();
        }

        return $translation;
    }

    private function initialiseAreaCode(): void
    {
        try {
            $this->state->setAreaCode(Area::AREA_FRONTEND);
        } catch (\Exception $e) {

        }
    }

    /**
     * @inheritDoc
     */
    public function getTranslationList($storeId)
    {
        $this->initialiseAreaCode();
        $result = [];

        try {
            $this->emulation->startEnvironmentEmulation($storeId, Area::AREA_FRONTEND, true);
            $list = $this->translate->getData();
            $this->emulation->stopEnvironmentEmulation();

            foreach ($list as $key => $value) {
                $result[] = [
                    'key' => $key,
                    'translation' => $value
                ];
            }
        } catch (\Exception $e) {
            $this->emulation->stopEnvironmentEmulation();
        }

        return $result;
    }
}
