<?php

namespace Mbs\TranslationApi;

class Logger
{
    private static $defaultLogFile = 'translationapi.log';

    public function addLog($message)
    {
        $this->writeLog($message);
    }

    private function writeLog(string $message, $file = '')
    {
        $writer = new \Zend\Log\Writer\Stream($this->getLogFile($file));
        $logger = new \Zend\Log\Logger();
        $logger->addWriter($writer);

        if ($message) {
            $logger->info($message);
        }
    }

    /**
     * @param $file
     * @return string
     */
    private function getLogFile($file)
    {
        if ($file == '') {
            $file = self::$defaultLogFile;
        }

        $logFileDir = BP . '/var/log/';

        if (!file_exists($logFileDir)) {
            mkdir($logFileDir, 0750, true);
        }

        return $logFileDir . $file;
    }
}